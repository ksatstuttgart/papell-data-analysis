# Papell Data Analysis

This Repository is for Papell Data analysis


How to use it:

install jupyter (easiest way probably anaconda):

 - download (https://www.anaconda.com/download/)
 - install
 - run anaconda prompt
   - run command: conda install -c conda-forge ipywidgets
 - start anaconda Navigator
 - start jupyter
 - follow the notebook inline commands